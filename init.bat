@echo off
echo.
echo Running init script

REM check existing files ------------------------------------------------------

if exist "docker-compose.yml" echo The file docker-compose.yml already exists. & goto:eof
if exist ".env" echo The file .env already exists. & goto:eof

REM copy files ----------------------------------------------------------------

echo Copy necessary files from init folder
copy init\docker-compose.yml docker-compose.yml
copy init\.env .env

REM set project name ----------------------------------------------------------

set damp_project_name=
echo.>>.env
echo # The project name (no space only underscore lowercase letters)>>.env
echo.
set /P damp_project_name="Enter project name: "
echo NAME=%damp_project_name%>>.env

:php
REM php version select --------------------------------------------------------

echo.>>.env
echo # PHP image version (must be an apache instance)>>.env

echo.
echo PHP version:
echo [1] PHP 7.2 
echo [2] PHP 7.4 (suggested)
echo [3] PHP 8.0

choice /C 123 /N /M "Select your preferred PHP version"
if errorlevel 3 echo PHP_IMAGE=php:8.0-apache>>.env & echo PHP_GD_INSTALL=docker-php-ext-configure gd --with-freetype --with-jpeg>>.env & goto:mysql
if errorlevel 2 echo PHP_IMAGE=php:7.4-apache>>.env & echo PHP_GD_INSTALL=docker-php-ext-configure gd --with-freetype --with-jpeg>>.env & goto:mysql
if errorlevel 1 echo PHP_IMAGE=php:7.2-apache>>.env & echo PHP_ZIP_INSTALL=docker-php-ext-configure zip --with-libzip>>.env & echo PHP_GD_INSTALL=docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/>>.env & goto:mysql

:mysql
REM mysql version select ------------------------------------------------------

echo.>>.env
echo # MySql image version>>.env

echo.
echo MySql version:
echo [1] MySql 5.6 
echo [2] MySql 5.7 
echo [3] MySql 8.0 (suggested)

choice /C 123 /N /M "Select your preferred MySql version"
if errorlevel 3 echo MYSQL_IMAGE=mysql:8.0>>.env & goto:mysqlpwd
if errorlevel 2 echo MYSQL_IMAGE=mysql:5.7>>.env & goto:mysqlpwd
if errorlevel 1 echo MYSQL_IMAGE=mysql:5.6>>.env & goto:mysqlpwd

:mysqlpwd
REM generate passwords --------------------------------------------------------

echo.>>.env
echo # MySql parameters>>.env
echo MYSQL_DATABASE=%damp_project_name%>>.env
echo MYSQL_USER=%damp_project_name%>>.env

setlocal enabledelayedexpansion
set "string=1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

set "result="
for /L %%i in (1,1,16) do call :add
echo MYSQL_ROOT_PASSWORD=%result%>>.env

set "result="
for /L %%i in (1,1,16) do call :add
echo MYSQL_PASSWORD=%result%>>.env

REM generate modifier scripts --------------------------------------------------

echo.
echo Generating script for modify necessary rights in filesystem
echo.
echo "finish.bat" for Windows host
echo.
echo docker exec %damp_project_name%-db chmod 644 /etc/mysql/conf.d/mysql-custom.cnf>finish.bat

REM finish script --------------------------------------------------------------

echo.
echo Docker environment scripts created.
echo Check the generated .env file, and run "docker-compose up" to start the container.
echo.
echo Edit host file if necessary.
Powershell Start Notepad.exe -ArgumentList "%SYSTEMROOT%\system32\drivers\etc\hosts" -Verb Runas

goto :eof

REM functions -----------------------------------------------------------------

:add
set /a x=%random% %% 62 
set result=%result%!string:~%x%,1!
goto :eof