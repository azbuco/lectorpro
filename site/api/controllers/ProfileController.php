<?php

namespace api\controllers;


use common\models\User;
use yii\rest\ActiveController;

class ProfileController extends ActiveController
{
    public $modelClass = User::class;
}