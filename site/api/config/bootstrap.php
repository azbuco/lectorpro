<?php

$container = Yii::$container;

//$container->set(yii\filters\auth\CompositeAuth::class, [
//    'authMethods' => [
//        yii\filters\auth\HttpBasicAuth::class,
//        yii\filters\auth\QueryParamAuth::class,
//    ],
//]);

$container->set(yii\filters\ContentNegotiator::class, [
    'formatParam' => 'format',
]);

$container->set(yii\rest\Serializer::class, [
    'collectionEnvelope' => 'items',
    'linksEnvelope' => 'links',
    'metaEnvelope' => 'meta',
]);
