<?php

namespace app\assets;

use yii\web\AssetBundle;

class BootstrapPluginAsset extends AssetBundle
{
    public $sourcePath = '@npm/bootstrap/dist';
    public $js = [
        'js/bootstrap.bundle.js'
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}