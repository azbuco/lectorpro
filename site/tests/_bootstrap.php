<?php
//define('YII_ENV', 'test');
//defined('YII_DEBUG') or define('YII_DEBUG', true);
//
//require_once __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
//require __DIR__ .'/../vendor/autoload.php';


defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');

// Search for autoload, since performance is irrelevant and usability isn't!
$dir = __DIR__ . '/..';

while (!file_exists($dir . '/vendor/autoload.php')) {
    if ($dir == dirname($dir)) {
        throw new \Exception('Failed to locate autoload.php');
    }
    $dir = dirname($dir);
}
defined('YII_APP_BASE_PATH') or define('YII_APP_BASE_PATH', $dir);
defined('VENDOR_DIR') or define('VENDOR_DIR', $dir . '/vendor');

require_once VENDOR_DIR . '/autoload.php';
require_once VENDOR_DIR . '/yiisoft/yii2/Yii.php';
require_once YII_APP_BASE_PATH . '/config/bootstrap.php';
require_once YII_APP_BASE_PATH . '/config/bootstrap-api.php';