<?php

use app\api\controllers\ProfileController;

class ProfileControllerCest
{
    public function responseFormats(\ApiTester $I)
    {
        $I->sendGet('profile');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseContainsJson(['success'=>true]);
    }
}
