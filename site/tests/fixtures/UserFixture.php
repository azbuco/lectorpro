<?php

namespace tests\fixtures;

use app\models\User;

class UserFixture extends \yii\test\ActiveFixture
{
    public $modelClass = User::class;
    public $dataFile = '@tests/fixtures/data/user.php';
}