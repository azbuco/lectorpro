<?php

return [
    'user0' => [
        'name' => 'Prof. Borbély Olivér PhD',
        'email' => 'dalma42@fulop.info',
        'password_hash' => '$2y$13$exgsr.gGOlL9kuUPhwh9HOyzfl0WITDeUOOfFOqiWCU0mOZu00hTK',
        'auth_key' => 'Bw0W3O3ps29nUaxys94bct2vg986Z8ne',
        'email_confirmation_token' => 'q_lCaWAEID082RYUF8RJ7ECUjFBak4ZV',
    ],
    'user1' => [
        'name' => 'Bakos Attiláné',
        'email' => 'katinka31@yahoo.com',
        'password_hash' => '$2y$13$BgYl6HJyyKEAS.yiwoLbn.tKe7evBRjg/kUB4AwGy/TxXyWP5WSEi',
        'auth_key' => 'ZkcmVYTBS56WZpiwqVobuHg2-EfVthcw',
        'email_confirmation_token' => '4P5mvkGZb6ygK4jjSRakMmVQ5m6huEV4',
    ],
];
