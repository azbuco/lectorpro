<?php

$config = [
    'id' => 'lectorpro-console',
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => \yii\console\controllers\MigrateController::class,
            'migrationPath' => [
                '@app/console/migrations',      // default migrations
                '@yii/rbac/migrations',         // yii rbac migrations
            ],
            'migrationNamespaces' => [
                'azbuco\user\migrations',
            ],
        ],
        'fixture' => [
            'class' => 'yii\faker\FixtureController',
            'namespace' => 'tests\fixtures',
            'templatePath' => '@tests/fixtures/templates',
            'fixtureDataPath' => '@tests/fixtures/data',
            'language' => 'hu_HU',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
