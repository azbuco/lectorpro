<?php


namespace common\models;

use azbuco\user\models\UserOptions;
use yii\db\ActiveQuery;

/**
 * @property UserOptions[] $userOptions
 */
class User extends \azbuco\user\models\User
{
    /**
     * Gets query for [[UserOptions]].
     *
     * @return ActiveQuery
     */
    public function getUserOptions()
    {
        return $this->hasMany(UserOptions::class, ['user_id' => 'id']);
    }

}