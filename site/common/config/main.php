<?php

return [
    'id' => 'lectorpro',
    'name' => 'Lector Pro',
    'sourceLanguage' => 'en',
    'language' => 'hu',
    'timeZone' => 'Europe/Budapest',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        'log',
        function() {
            // there is a component called user, so we specify the user module
            return Yii::$app->getModule('user');
        },
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            // uncomment if you want to cache RBAC items hierarchy
            // 'cache' => 'cache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => azbuco\user\models\User::class,
            'enableAutoLogin' => true,
            'loginUrl' => ['/user/security/login'],
        ],
    ],
    'modules' => [
        'user' => [
            'class' => azbuco\user\UserModule::class,
            'requireEmailConfirmation' => false,
            'enableRegistration' => true,
        ],
    ],
    'params' => [],
];
